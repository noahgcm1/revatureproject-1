package dev.mayers.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import com.google.gson.Gson;

import dev.mayers.models.ApprovalStatus;
import dev.mayers.models.Department;
import dev.mayers.models.Employee;
import dev.mayers.models.Form;
import dev.mayers.services.ApprovalStatusService;
import dev.mayers.services.ApprovalStatusServiceImpl;
import dev.mayers.services.DepartmentService;
import dev.mayers.services.DepartmentServiceImpl;
import dev.mayers.services.EmployeeService;
import dev.mayers.services.EmployeeServiceImpl;
import dev.mayers.services.FormService;
import dev.mayers.services.FormServiceImpl;

public class DashboardController {
	
	private static EmployeeService es = new EmployeeServiceImpl();
	private static FormService fs = new FormServiceImpl();
	private static ApprovalStatusService ass = new ApprovalStatusServiceImpl();
	private static DepartmentService ds = new DepartmentServiceImpl();
	private static Gson gson = new Gson();
	
	public static void getUserForms(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		List<Form> userForms = fs.getFormsByEmployee(es.getEmployee(id));
		
		List<Pair<Form, ApprovalStatus>> finalForms = new ArrayList<Pair<Form, ApprovalStatus>>();
		for(Form f : userForms) 
			finalForms.add(Pair.with(f, ass.getApprovalStatus(f)));
		
		response.getWriter().append((finalForms.size() > 0) ? gson.toJson(finalForms) : "[]");
	}
	
	public static void getSupervisorForms(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		boolean benco = id == getBenCoID();
		List<Form> allForms = fs.getAllForms();
		
		List<Triplet<Form, ApprovalStatus, Integer>> pendingForms = new ArrayList<Triplet<Form, ApprovalStatus, Integer>>();
		for(Form f : allForms) {
			int userType = determineSupervisorType(id, f, benco);
			if(userType > 0)
				pendingForms.add(Triplet.with(f, ass.getApprovalStatus(f), userType));
		}
		response.getWriter().append((pendingForms.size() > 0) ? gson.toJson(pendingForms) : "[]");
	}
	
	public static void getUserAndForm(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Form form = fs.getForm(id);
		Employee employee = es.getEmployee(form.getEmployeeId());
		
		Pair<Form, Employee> tuple = Pair.with(form, employee);
		response.getWriter().append(gson.toJson(tuple));
	}
	
	private static int getBenCoID() {
		List<Department> departments = ds.getAllDepartments();
		for(Department d : departments) 
			if(d.getName().equals("Benefits"))
				return d.getHeadId();
		return -1;
	}
	
	private static int determineSupervisorType(int id, Form form, boolean benco) {
		Employee e = es.getEmployee(form.getEmployeeId());
		ApprovalStatus a = ass.getApprovalStatus(form);
		int type = 0;
		
		/*
		 *  0 - not valid supervisor OR form is not on the proper stage for approval
		 *  1 - direct supervisor
		 *  2 - department head
		 *  4 - benefits coordinator
		 */
		
		//check that the input id is a direct supervisor of the form and that the relevant stage hasn't been passed
		if(e.getSupervisorId() == id) 
			if(!("APPROVED DECLINED".contains(a.getDirectSupervisor())))
				type += 1;
		
		//same as above but also checks that the relevant stage has been reached
		if(es.getDepartmentHead(e).getId() == id) 
			if("APPROVED".equals(a.getDirectSupervisor()) || type > 0)
				if(!("APPROVED DECLINED".contains(a.getDepartmentHead())))
					type += 2;
		
		//same as above
		if(benco) 
			if("APPROVED".equals(a.getDepartmentHead()))
				if(!("APPROVED DECLINED".contains(a.getBenefitsCoordinator())))
					type += 4;
		
		return type;
	}
}