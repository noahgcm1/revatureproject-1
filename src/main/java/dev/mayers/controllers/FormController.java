package dev.mayers.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import dev.mayers.models.ApprovalStatus;
import dev.mayers.models.Employee;
import dev.mayers.models.Form;
import dev.mayers.services.ApprovalStatusService;
import dev.mayers.services.ApprovalStatusServiceImpl;
import dev.mayers.services.EmployeeService;
import dev.mayers.services.EmployeeServiceImpl;
import dev.mayers.services.FormService;
import dev.mayers.services.FormServiceImpl;

public class FormController {
	private static Gson gson = new Gson();
	private static FormService fs = new FormServiceImpl();
	private static ApprovalStatusService ass = new ApprovalStatusServiceImpl();
	private static EmployeeService es = new EmployeeServiceImpl();
	
	public static void addForm(HttpServletRequest request, HttpServletResponse response) throws JsonSyntaxException, JsonIOException, IOException {
		Form form = gson.fromJson(request.getReader(), Form.class);
		System.out.println(form);
		fs.addForm(form);
		
		Employee employee = es.getEmployee(form.getEmployeeId());
		employee.setTuitionPending(fs.calculatePendingTuition(employee));
	}
	
	public static void getForm(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Form f = fs.getForm(Integer.parseInt(request.getParameter("id")));
		response.getWriter().append((f != null) ? gson.toJson(f) : "{}");
	}
	
	public static void editForm(HttpServletRequest request, HttpServletResponse response) throws JsonSyntaxException, JsonIOException, IOException {
		Form oldForm = fs.getForm(Integer.parseInt(request.getParameter("id")));
		Form form = gson.fromJson(request.getReader(), Form.class);
				
		oldForm.setCost(form.getCost());
		oldForm.setDescription(form.getDescription());
		oldForm.setFormat(form.getFormat());
		oldForm.setGrade(form.getGrade());
		oldForm.setJustification(form.getJustification());
		oldForm.setLocation(form.getJustification());
		oldForm.setPassingGrade(form.getPassingGrade());
		oldForm.setReimbursement(form.getReimbursement());
		oldForm.setTimestamp(form.getTimestamp());
		oldForm.setType(form.getType());
		
		fs.updateForm(oldForm);
	}

	public static void removeForm(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request.getParameter("id"));
		Form form = fs.getForm(Integer.parseInt(request.getParameter("id")));
		fs.deleteForm(form);
		ass.deleteApprovalStatus(ass.getApprovalStatus(form));
	}

	public static void respondForm(HttpServletRequest request, HttpServletResponse response) {
		int userType = Integer.parseInt(request.getParameter("userType"));
		Form form = fs.getForm(Integer.parseInt(request.getParameter("id")));
		ApprovalStatus status = ass.getApprovalStatus(fs.getForm(Integer.parseInt(request.getParameter("id"))));
		String message = request.getParameter("message");
		
			if(userType >= 4) {
				userType -= 4;
				status.setBenefitsCoordinator(message);
			}
			if(userType >= 2) {
				userType -= 2;
				status.setDepartmentHead(message);
			}
			if(userType >= 1) {
				status.setDirectSupervisor(message);
			}
		
			cascadeStatus(status);
			ass.updateApprovalStatus(status);
			
			if(status.getBenefitsCoordinator().equals("APPROVED"))
				reimburse(form);
	}
	
	private static void reimburse(Form form) {
		Employee e = es.getEmployee(form.getEmployeeId());
		e.setTuitionClaimed(e.getTuitionClaimed() + form.getCost());
		ass.deleteApprovalStatus(ass.getApprovalStatus(form));
		fs.deleteForm(form);
		
		e.setTuitionPending(fs.calculatePendingTuition(e));
	}
	
	private static void cascadeStatus(ApprovalStatus status) {
		if(!status.getBenefitsCoordinator().equals("PENDING") && status.getDepartmentHead().equals("PENDING"))
			status.setDepartmentHead(status.getBenefitsCoordinator());
		
		if(!status.getDepartmentHead().equals("PENDING") && status.getDirectSupervisor().equals("PENDING"))
			status.setDirectSupervisor(status.getDepartmentHead());
	}
}