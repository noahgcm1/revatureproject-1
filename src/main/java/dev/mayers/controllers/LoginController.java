package dev.mayers.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dev.mayers.models.Employee;
import dev.mayers.services.EmployeeService;
import dev.mayers.services.EmployeeServiceImpl;

public class LoginController {
	private static EmployeeService es = new EmployeeServiceImpl();
	private static Gson gson = new Gson();
	
	public static void tryLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String input = request.getParameter("user");
		List<Employee> employees = es.getAllEmployees();
		
		Employee user = null;
		for(Employee e: employees)
			if(e.getEmail().equals(input)) 
				user = e;
		
		response.getWriter().append((user != null) ? gson.toJson(user) : "{}");
	}
	
	public static void getUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int id = Integer.parseInt(request.getParameter("user"));
		
		System.out.println(id);
		response.getWriter().append((id != -1) ? gson.toJson(es.getEmployee(id)) : "{}");
	}
}
