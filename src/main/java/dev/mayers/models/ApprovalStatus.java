package dev.mayers.models;

public class ApprovalStatus {
	private int id;
	private String directSupervisor;
	private String departmentHead;
	private String benefitsCoordinator;
	
	public ApprovalStatus() {
		super();
	}

	public ApprovalStatus(int id, String directSupervisor, String departmentHead, String benefitsCoordinator) {
		super();
		this.id = id;
		this.directSupervisor = directSupervisor;
		this.departmentHead = departmentHead;
		this.benefitsCoordinator = benefitsCoordinator;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDirectSupervisor() {
		return directSupervisor;
	}

	public void setDirectSupervisor(String directSupervisor) {
		this.directSupervisor = directSupervisor;
	}

	public String getDepartmentHead() {
		return departmentHead;
	}

	public void setDepartmentHead(String departmentHead) {
		this.departmentHead = departmentHead;
	}

	public String getBenefitsCoordinator() {
		return benefitsCoordinator;
	}

	public void setBenefitsCoordinator(String benefitsCoordinator) {
		this.benefitsCoordinator = benefitsCoordinator;
	}

	@Override
	public String toString() {
		return "ApprovalStatus [id=" + id + ", directSupervisor=" + directSupervisor + ", departmentHead="
				+ departmentHead + ", benefitsCoordinator=" + benefitsCoordinator + "]";
	}
}
