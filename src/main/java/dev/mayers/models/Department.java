package dev.mayers.models;

public class Department {
	private int id;
	private String name;
	private int headId;
	
	public Department() {
		super();
	}

	public Department( String name, int headId) {
		super();
		this.name = name;
		this.headId = headId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHeadId() {
		return headId;
	}

	public void setHeadId(int headId) {
		this.headId = headId;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", headId=" + headId + "]";
	}
}
