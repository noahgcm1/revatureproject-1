package dev.mayers.models;

public class Employee {
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private int tuitionClaimed;
	private int tuitionPending;
	private int supervisorId;
	private int departmentId;
	
	public Employee() {
		super();
	}

	public Employee(int id, String firstName, String lastName, String email, String password, int tuitionClaimed,
			int tutionPending, int supervisorId, int departmentId) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.tuitionClaimed = tuitionClaimed;
		this.tuitionPending = tuitionPending;
		this.supervisorId = supervisorId;
		this.departmentId = departmentId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTuitionClaimed() {
		return tuitionClaimed;
	}

	public void setTuitionClaimed(int tuitionClaimed) {
		this.tuitionClaimed = tuitionClaimed;
	}
	
	public int getTuitionPending() {
		return tuitionPending;
	}

	public void setTuitionPending(int tutionPending) {
		this.tuitionPending = tutionPending;
	}

	public int getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(int supervisorId) {
		this.supervisorId = supervisorId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", tuitionClaimed=" + tuitionClaimed + ", tutionPending=" + tuitionPending
				+ ", supervisorId=" + supervisorId + ", departmentId=" + departmentId + "]";
	}
}
