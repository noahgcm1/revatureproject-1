package dev.mayers.models;

public class Event {
	private int id;
	private String grade;
	private String type;
	
	public Event() {
		super();
	}

	public Event(int id, String grade, String type) {
		super();
		this.id = id;
		this.grade = grade;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", grade=" + grade + ", type=" + type + "]";
	}
}
