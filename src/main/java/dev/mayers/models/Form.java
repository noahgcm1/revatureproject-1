package dev.mayers.models;

public class Form {
	private int id;
	private String timestamp;
	private String location;
	private String description;
	private int cost;
	private int reimbursement;
	private String format;
	private String grade;
	private String passingGrade;
	private String type;
	private String justification;
	private int eventId;
	private int employeeId;
	private int approvalStatusId;
	
	public Form() {
		super();
	}

	public Form(int id, String timestamp, String location, String description, int cost, int reimbursement,
			String format, String grade, String passingGrade, String type, String justification, int eventId,
			int employeeId, int approvalStatusId) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.location = location;
		this.description = description;
		this.cost = cost;
		this.reimbursement = reimbursement;
		this.format = format;
		this.grade = grade;
		this.passingGrade = passingGrade;
		this.type = type;
		this.justification = justification;
		this.eventId = eventId;
		this.employeeId = employeeId;
		this.approvalStatusId = approvalStatusId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getPassingGrade() {
		return passingGrade;
	}

	public void setPassingGrade(String passingGrade) {
		this.passingGrade = passingGrade;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public int getReimbursement() {
		return reimbursement;
	}

	public void setReimbursement(int reimbursement) {
		this.reimbursement = reimbursement;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getApprovalStatusId() {
		return approvalStatusId;
	}

	public void setApprovalStatusId(int approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}

	@Override
	public String toString() {
		return "Form [id=" + id + ", timestamp=" + timestamp + ", location=" + location + ", description=" + description
				+ ", cost=" + cost + ", reimbursement=" + reimbursement + ", format=" + format + ", grade=" + grade
				+ ", passingGrade=" + passingGrade + ", type=" + type + ", justification=" + justification
				+ ", eventId=" + eventId + ", employeeId=" + employeeId + ", approvalStatusId=" + approvalStatusId
				+ "]";
	}
}
