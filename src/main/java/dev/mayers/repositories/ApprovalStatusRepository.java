package dev.mayers.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.models.ApprovalStatus;

public class  ApprovalStatusRepository implements Repository<ApprovalStatus>{

	public static Connection conn = JDBCConnection.getConnection();

	@Override
	public boolean add(ApprovalStatus e) {
		try {
	    	String sql = "CALL add_approval_status(?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, Integer.toString(0));
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException sqle) {
    		sqle.printStackTrace();
    	}
        return false;
	}
	
	@Override
	public ApprovalStatus get(int id) {
		try {
	    	String sql = "SELECT * FROM approval_status_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                ApprovalStatus e = new ApprovalStatus();
                e.setId(rs.getInt("ID"));
                e.setDirectSupervisor(rs.getString("DIRECT_SUPERVISOR"));
                e.setDepartmentHead(rs.getString("DEPARTMENT_HEAD"));
                e.setBenefitsCoordinator(rs.getString("BENEFITS_COORDINATOR"));
                return e;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
	}

	@Override
	public List<ApprovalStatus> getAll() {
		try {			
    		String sql = "SELECT * FROM approval_status_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<ApprovalStatus> list = new ArrayList<ApprovalStatus>();		
    		while(rs.next()) {
    			ApprovalStatus e = new ApprovalStatus();
                e.setId(rs.getInt("ID"));
                e.setDirectSupervisor(rs.getString("DIRECT_SUPERVISOR"));
                e.setDepartmentHead(rs.getString("DEPARTMENT_HEAD"));
                e.setBenefitsCoordinator(rs.getString("BENEFITS_COORDINATOR"));
    			list.add(e);
    		}			
    		return list;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(ApprovalStatus e) {
		try {
			String sql = "UPDATE approval_status_table SET direct_supervisor = ?, department_head = ?, benefits_coordinator = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, e.getDirectSupervisor());
			ps.setString(2, e.getDepartmentHead());
			ps.setString(3, e.getBenefitsCoordinator());
			ps.setString(4, Integer.toString(e.getId()));
			
			ps.executeQuery();
			
			return true;

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM approval_status_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}