package dev.mayers.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.models.Department;

public class DepartmentRepository implements Repository<Department>{

	public static Connection conn = JDBCConnection.getConnection();
	
	@Override
	public boolean add(Department e) {
		try {
	    	String sql = "CALL add_department(?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, e.getName());
	    	cs.setString(2, Integer.toString(e.getHeadId()));
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException sqle) {
    		sqle.printStackTrace();
    	}
        return false;
	}
	
	@Override
	public Department get(int id) {
		try {
	    	String sql = "SELECT * FROM department_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Department e = new Department();
                e.setId(rs.getInt("ID"));
                e.setName(rs.getString("NAME"));
                e.setHeadId(rs.getInt("HEAD"));
                return e;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
	}

	@Override
	public List<Department> getAll() {
		try {			
    		String sql = "SELECT * FROM department_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Department> list = new ArrayList<Department>();		
    		while(rs.next()) {
    			Department e = new Department();
    			e.setId(rs.getInt("ID"));
                e.setName(rs.getString("NAME"));
                e.setHeadId(rs.getInt("HEAD"));
    			list.add(e);
    		}			
    		return list;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(Department e) {
		try {
			String sql = "UPDATE department_table SET name = ?, head = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, e.getName());
			ps.setString(2, Integer.toString(e.getHeadId()));
			ps.setString(3, Integer.toString(e.getId()));
			
			ps.executeQuery();
			
			return true;

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM department_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}