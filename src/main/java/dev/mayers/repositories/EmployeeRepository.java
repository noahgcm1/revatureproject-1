package dev.mayers.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.models.Employee;

public class EmployeeRepository implements Repository<Employee>{

	public static Connection conn = JDBCConnection.getConnection();

	@Override
	public boolean add(Employee e) {
		try {
	    	String sql = "CALL add_employee(?,?,?,?,?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, e.getFirstName());
	    	cs.setString(2, e.getLastName());
	    	cs.setString(3, e.getEmail());
	    	cs.setString(4, e.getPassword());
	    	cs.setString(5, Integer.toString(e.getSupervisorId()));
	    	cs.setString(6, Integer.toString(e.getDepartmentId()));
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException sqle) {
    		sqle.printStackTrace();
    	}
        return false;
	}
	
	@Override
	public Employee get(int id) {
		try {
	    	String sql = "SELECT * FROM employee_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Employee e = new Employee();
                e.setId(rs.getInt("ID"));
                e.setFirstName(rs.getString("FIRST_NAME"));
                e.setLastName(rs.getString("LAST_NAME"));
                e.setEmail(rs.getString("EMAIL_ADDRESS"));
                e.setPassword(rs.getString("PASSWORD"));
                e.setTuitionClaimed(rs.getInt("TUTITION_CLAIMED"));
                e.setTuitionPending(rs.getInt("TUITION_PENDING"));
                e.setSupervisorId(rs.getInt("SUPERVISOR"));
                e.setDepartmentId(rs.getInt("DEPARTMENT"));
                
                return e;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
	}
	
	@Override
	public List<Employee> getAll() {
		try {			
    		String sql = "SELECT * FROM employee_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Employee> list = new ArrayList<Employee>();		
    		while(rs.next()) {
    			Employee e = new Employee();
                e.setId(rs.getInt("ID"));
                e.setFirstName(rs.getString("FIRST_NAME"));
                e.setLastName(rs.getString("LAST_NAME"));
                e.setEmail(rs.getString("EMAIL_ADDRESS"));
                e.setPassword(rs.getString("PASSWORD"));
                e.setTuitionClaimed(rs.getInt("TUTITION_CLAIMED"));
                e.setTuitionPending(rs.getInt("TUITION_PENDING"));
                e.setSupervisorId(rs.getInt("SUPERVISOR"));
                e.setDepartmentId(rs.getInt("DEPARTMENT"));
                
                list.add(e);
    		}			
    		return list;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(Employee e) {
		try {
			String sql = "UPDATE employee_table SET first_name = ?, last_name = ?, email_address = ?, password = ?, tuition_claimed = ?, supervisor = ?, department = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, e.getFirstName());
			ps.setString(2, e.getLastName());
			ps.setString(3, e.getEmail());
			ps.setString(4, e.getPassword());
			ps.setString(5, Integer.toString(e.getTuitionClaimed()));
			ps.setString(6, Integer.toString(e.getSupervisorId()));
			ps.setString(7, Integer.toString(e.getDepartmentId()));
			ps.setString(8, Integer.toString(e.getId()));
			
			ps.executeQuery();
			
			return true;

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM employee_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}