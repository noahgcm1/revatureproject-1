package dev.mayers.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.models.Event;

public class EventRepository  implements Repository<Event>{

	public static Connection conn = JDBCConnection.getConnection();

	@Override
	public boolean add(Event e) {
		try {
	    	String sql = "CALL add_event(?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, e.getGrade());
	    	cs.setString(2, e.getType());
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException sqle) {
    		sqle.printStackTrace();
    	}
        return false;
	}
	
	@Override
	public Event get(int id) {
		try {
	    	String sql = "SELECT * FROM event_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Event e = new Event();
                e.setId(rs.getInt("ID"));
                e.setGrade(rs.getString("GRADE"));
                e.setType(rs.getString("TYPE"));
                return e;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
	}

	@Override
	public List<Event> getAll() {
		try {			
    		String sql = "SELECT * FROM event_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Event> list = new ArrayList<Event>();		
    		while(rs.next()) {
    			Event e = new Event();
                e.setId(rs.getInt("ID"));
                e.setGrade(rs.getString("GRADE"));
                e.setType(rs.getString("TYPE"));
                
                list.add(e);
    		}			
    		return list;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(Event e) {
		try {
			String sql = "UPDATE event_table SET grade = ?, type = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, e.getGrade());
			ps.setString(2, e.getType());
			ps.setString(3, Integer.toString(e.getId()));
			
			ps.executeQuery();
			
			return true;

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM event_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}