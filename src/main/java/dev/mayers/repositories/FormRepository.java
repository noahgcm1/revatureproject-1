package dev.mayers.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.models.Form;

public class FormRepository implements Repository<Form>{

	public static Connection conn = JDBCConnection.getConnection();

	@Override
	public boolean add(Form e) {
		try {
	    	String sql = "CALL add_form(?,?,?,?,?,?,?,?,?,?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, e.getTimestamp());
	    	cs.setString(2, e.getLocation());
	    	cs.setString(3, e.getDescription());
	    	cs.setString(4, Integer.toString(e.getCost()));
	    	cs.setString(5, Integer.toString(e.getReimbursement()));
	    	cs.setString(6, e.getFormat());
	    	cs.setString(7, e.getGrade());
	    	cs.setString(8, e.getPassingGrade());
	    	cs.setString(9, e.getType());
	    	cs.setString(10, e.getJustification());
	    	cs.setString(11, Integer.toString(e.getEmployeeId()));
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException sqle) {
    		sqle.printStackTrace();
    	}
        return false;
	}
	
	@Override
	public Form get(int id) {
		try {
	    	String sql = "SELECT * FROM form_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Form e = new Form();
                e.setId(rs.getInt("ID"));
                e.setTimestamp(rs.getString("TIMESTAMP"));
                e.setLocation(rs.getString("LOCATION"));
                e.setDescription(rs.getString("DESCRIPTION"));
                e.setCost(rs.getInt("COST"));
                e.setReimbursement(rs.getInt("REIMBURSEMENT"));
                e.setFormat(rs.getString("GRADING_FORMAT"));
                e.setGrade(rs.getString("GRADE"));
                e.setPassingGrade(rs.getString("PASSING_GRADE"));
                e.setType(rs.getString("EVENT_TYPE"));
                e.setJustification(rs.getString("JUSTIFICATION"));
                e.setEventId(rs.getInt("EVENT"));
                e.setEmployeeId(rs.getInt("EMPLOYEE"));
                e.setApprovalStatusId(rs.getInt("APPROVAL_STATUS"));
                return e;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
    }
	
	@Override
	public List<Form> getAll() {
		try {			
    		String sql = "SELECT * FROM form_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Form> list = new ArrayList<Form>();		
    		while(rs.next()) {
    			 Form e = new Form();
                 e.setId(rs.getInt("ID"));
                 e.setTimestamp(rs.getString("TIMESTAMP"));
                 e.setLocation(rs.getString("LOCATION"));
                 e.setDescription(rs.getString("DESCRIPTION"));
                 e.setCost(rs.getInt("COST"));
                 e.setReimbursement(rs.getInt("REIMBURSEMENT"));
                 e.setFormat(rs.getString("GRADING_FORMAT"));
                 e.setGrade(rs.getString("GRADE"));
                 e.setPassingGrade(rs.getString("PASSING_GRADE"));
                 e.setType(rs.getString("EVENT_TYPE"));
                 e.setJustification(rs.getString("JUSTIFICATION"));
                 e.setEventId(rs.getInt("EVENT"));
                 e.setEmployeeId(rs.getInt("EMPLOYEE"));
                 e.setApprovalStatusId(rs.getInt("APPROVAL_STATUS"));
                
                list.add(e);
    		}			
    		return list;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(Form e) {
		try {
			String sql = "UPDATE form_table SET timestamp = ?, location = ?, description = ?, cost = ?, grading_format = ?, event_type = ?, justification = ?, event = ?, employee = ?, approval_status = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, e.getTimestamp());
			ps.setString(2, e.getLocation());
			ps.setString(3, e.getDescription());
			ps.setString(4, Integer.toString(e.getCost()));
			ps.setString(5, e.getFormat());
			ps.setString(6, e.getType());
			ps.setString(7, e.getJustification());
			ps.setString(8, Integer.toString(e.getEventId()));
			ps.setString(9, Integer.toString(e.getEmployeeId()));
			ps.setString(10, Integer.toString(e.getApprovalStatusId()));
			ps.setString(11, Integer.toString(e.getId()));
			
			ps.executeQuery();
			
			return true;

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM form_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}