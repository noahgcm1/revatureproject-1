package dev.mayers.repositories;

import java.util.List;

public interface Repository<E> {

	public E get(int id);

	public boolean add(E e);

	public List<E> getAll();

	public boolean update(E e);

	public boolean delete(int id);

}
