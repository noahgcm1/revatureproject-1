package dev.mayers.services;

import java.util.List;
import dev.mayers.models.ApprovalStatus;
import dev.mayers.models.Form;

public interface ApprovalStatusService {
	public boolean addApprovalStatus(ApprovalStatus ApprovalStatus);
	public ApprovalStatus getApprovalStatus(int id);
	public ApprovalStatus getApprovalStatus(Form form);
	public List<ApprovalStatus> getAllApprovalStatuss();
	public boolean updateApprovalStatus(ApprovalStatus status);
	public boolean deleteApprovalStatus(ApprovalStatus status);
}