package dev.mayers.services;

import java.util.List;

import dev.mayers.models.ApprovalStatus;
import dev.mayers.models.Form;
import dev.mayers.repositories.ApprovalStatusRepository;

public class ApprovalStatusServiceImpl implements ApprovalStatusService{
	
	private ApprovalStatusRepository asrepo = new ApprovalStatusRepository();

	@Override
	public boolean addApprovalStatus(ApprovalStatus approvalstatus) {
		return asrepo.add(approvalstatus);
	}

	@Override
	public ApprovalStatus getApprovalStatus(int id) {
		return asrepo.get(id);
	}

	@Override
	public ApprovalStatus getApprovalStatus(Form form) {
		return getApprovalStatus(form.getApprovalStatusId());
	}

	@Override
	public List<ApprovalStatus> getAllApprovalStatuss() {
		return asrepo.getAll();
	}

	@Override
	public boolean updateApprovalStatus(ApprovalStatus status) {
		return asrepo.update(status);
	}

	@Override
	public boolean deleteApprovalStatus(ApprovalStatus status) {
		return asrepo.delete(status.getId());
	}
}
