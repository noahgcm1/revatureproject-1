package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Department;
import dev.mayers.models.Employee;

public interface DepartmentService {
	public boolean addDepartment(Department Department);
	public Department getDepartment(int id);
	public Department getDepartment(Employee employee);
	public List<Department> getAllDepartments();
	public boolean updateDepartment(Department status);
	public boolean deleteDepartment(Department status);
}