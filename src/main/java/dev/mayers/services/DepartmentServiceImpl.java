package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Department;
import dev.mayers.models.Employee;
import dev.mayers.repositories.DepartmentRepository;

public class DepartmentServiceImpl implements DepartmentService{

	public DepartmentRepository deprepo = new DepartmentRepository();
	
	@Override
	public boolean addDepartment(Department department) {
		return deprepo.add(department);
	}

	@Override
	public Department getDepartment(int id) {
		return deprepo.get(id);
	}

	@Override
	public Department getDepartment(Employee employee) {
		return deprepo.get(employee.getDepartmentId());
	}

	@Override
	public List<Department> getAllDepartments() {
		return deprepo.getAll();
	}

	@Override
	public boolean updateDepartment(Department status) {
		return deprepo.update(status);
	}

	@Override
	public boolean deleteDepartment(Department status) {
		return deprepo.delete(status.getId());
	}
}
