package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Employee;

public interface EmployeeService {
	public boolean addEmployee(Employee employee);
	public Employee getEmployee(int id);
	public Employee getSupervisor(Employee employee);
	public Employee getSupervisor(int id);
	public Employee getDepartmentHead(Employee employee);
	public Employee getDepartmentHead(int id);
	public List<Employee> getAllEmployees();
	public boolean updateEmployee(Employee status);
	public boolean deleteEmployee(Employee status);
}