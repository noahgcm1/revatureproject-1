package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Department;
import dev.mayers.models.Employee;
import dev.mayers.repositories.EmployeeRepository;

public class EmployeeServiceImpl implements EmployeeService{

	public EmployeeRepository emprepo = new EmployeeRepository();
	
	@Override
	public boolean addEmployee(Employee employee) {
		return emprepo.add(employee);
	}

	@Override
	public Employee getEmployee(int id) {
		return emprepo.get(id);
	}

	@Override
	public Employee getSupervisor(Employee employee) {
		return getEmployee(employee.getSupervisorId());
	}

	@Override
	public Employee getDepartmentHead(Employee employee) {
		DepartmentService ds = new DepartmentServiceImpl();
		Department department = ds.getDepartment(employee);
		return getEmployee(department.getHeadId());
	}

	@Override
	public List<Employee> getAllEmployees() {
		return emprepo.getAll();
	}

	@Override
	public boolean updateEmployee(Employee employee) {
		return emprepo.update(employee);
	}

	@Override
	public boolean deleteEmployee(Employee employee) {
		return emprepo.delete(employee.getId());
	}

	@Override
	public Employee getSupervisor(int id) {
		return getSupervisor(emprepo.get(id));
	}

	@Override
	public Employee getDepartmentHead(int id) {
		return getDepartmentHead(emprepo.get(id));
	}
}
