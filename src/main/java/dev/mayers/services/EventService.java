package dev.mayers.services;

import java.util.List;
import dev.mayers.models.Event;
import dev.mayers.models.Form;

public interface EventService {
	public boolean addEvent(Event event);
	public Event getEvent(int id);
	public Event getEvent(Form form);
	public List<Event> getAllEvents();
	public boolean updateEvent(Event event);
	public boolean deleteEvent(Event event);
}
