package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Event;
import dev.mayers.models.Form;
import dev.mayers.repositories.EventRepository;

public class EventServiceImpl implements EventService{
	
	EventRepository everepo = new EventRepository();

	@Override
	public boolean addEvent(Event event) {
		return everepo.add(event);
	}

	@Override
	public Event getEvent(int id) {
		return everepo.get(id);
	}

	@Override
	public Event getEvent(Form form) {
		return getEvent(form.getEventId());
	}

	@Override
	public List<Event> getAllEvents() {
		return everepo.getAll();
	}

	@Override
	public boolean updateEvent(Event event) {
		return everepo.update(event);
	}

	@Override
	public boolean deleteEvent(Event event) {
		return everepo.delete(event.getId());
	}
}