package dev.mayers.services;

import java.util.List;

import dev.mayers.models.Form;
import dev.mayers.models.Employee;

public interface FormService {
	public boolean addForm(Form Form);
	public Form getForm(int id);
	public List<Form> getFormsByEmployee(Employee employee);
	public List<Form>getFormsBySupervisor(Employee employee);
	public List<Form> getAllForms();
	public boolean updateForm(Form status);
	public boolean deleteForm(Form status);

	public int calculatePendingTuition(Employee employee);
}