package dev.mayers.services;

import java.util.Iterator;
import java.util.List;

import dev.mayers.models.Employee;
import dev.mayers.models.Form;
import dev.mayers.repositories.FormRepository;

public class FormServiceImpl implements FormService{
	
	FormRepository formrepo = new FormRepository();

	@Override
	public boolean addForm(Form form) {
		return formrepo.add(form);
	}

	@Override
	public Form getForm(int id) {
		return formrepo.get(id);
	}

	@Override
	public List<Form> getFormsByEmployee(Employee employee) {
		List<Form> forms = formrepo.getAll();
		
		//evidently for each doesn't play nice with remove
		Iterator<Form> f = forms.iterator();
		while(f.hasNext())
			if(f.next().getEmployeeId() != employee.getId())
				f.remove();
		
		return forms;
	}

	@Override
	public List<Form> getFormsBySupervisor(Employee employee) {
		List<Form> forms = formrepo.getAll();
		EmployeeService es = new EmployeeServiceImpl();
		
		Iterator<Form> f = forms.iterator();
		while(f.hasNext())
			if(es.getSupervisor(f.next().getEmployeeId()).getId() != employee.getId())
				f.remove();
		
		return forms;
	}

	@Override
	public List<Form> getAllForms() {
		return formrepo.getAll();
	}

	@Override
	public boolean updateForm(Form form) {
		return formrepo.update(form);
	}

	@Override
	public boolean deleteForm(Form form) {
		return formrepo.delete(form.getId());
	}

	@Override
	public int calculatePendingTuition(Employee employee) {
		List<Form> forms = getFormsByEmployee(employee);
		
		int pendingTuition = 0;
		for(Form f : forms) 
			pendingTuition += f.getReimbursement();
		
		return pendingTuition;
	}
}
