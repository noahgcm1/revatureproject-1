package dev.mayers.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.mayers.controllers.*;

public class RequestHelper {
	
	/**
	 * This method will delegate other methods on the controller
	 * layer of our application to process the request.
	 * @param request the HTTP Request
	 * @param response the HTTP Response
	 * @throws IOException 
	 */
	public static void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String uri = request.getRequestURI();
		System.out.println(uri);
		
		switch (uri) {
			case "/Project1-TRMS/tryLogin.do": 
				LoginController.tryLogin(request, response);
				break;
			case "/Project1-TRMS/getUser.do":
				LoginController.getUser(request, response);
				break;
			case "/Project1-TRMS/getUserForms.do":
				DashboardController.getUserForms(request, response);
				break;
			case "/Project1-TRMS/getSupervisorForms.do":
				DashboardController.getSupervisorForms(request, response);
				break;
			case "/Project1-TRMS/addForm.do":
				FormController.addForm(request, response);
				break;	
			case "/Project1-TRMS/getForm.do":
				FormController.getForm(request, response);
				break;
			case "/Project1-TRMS/editForm.do":
				FormController.editForm(request, response);
				break;
			case "/Project1-TRMS/removeForm.do":
				FormController.removeForm(request, response);
				break;
			case "/Project1-TRMS/respondForm.do":
				FormController.respondForm(request,response);
				break;
			case "/Project1-TRMS/getUserAndForm.do":
				DashboardController.getUserAndForm(request, response);
				break;
			default: 
				response.sendError(418, "bruh.");
		}
		
	}
}