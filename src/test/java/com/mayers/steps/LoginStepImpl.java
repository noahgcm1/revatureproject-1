package com.mayers.steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dev.mayers.pages.DashboardPage;
import dev.mayers.pages.LoginPage;
import dev.mayers.runners.LoginRunner;

public class LoginStepImpl {
	
	public static LoginPage login = LoginRunner.login;
	public static DashboardPage dashboard = LoginRunner.dashboard;
	public static WebDriver driver = LoginRunner.driver;
	
	@Given("^The Guest is on the login page$")
	public void the_guest_is_on_the_login_page() {
		driver.get("http://localhost:8080/Project1-TRMS/");
	}
	
	//"([^"]*)"
	@When("^The user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void the_user_enters_and(String address, String pass) {
		login.emailBar.sendKeys(address);
		login.passwordBar.sendKeys(pass);
		login.loginButton.click();
	}

	@Then("^The name shown should be \"([^\"]*)\"$")
	public void the_name_shown_should_be(String name) {
		Assert.assertTrue(dashboard.nameLabel.toString().contains(name));
	}
}
