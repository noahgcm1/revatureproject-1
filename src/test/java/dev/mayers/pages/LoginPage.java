package dev.mayers.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage{
	
	public WebDriver driver;
	
	@FindBy(id = "loginbutton")
	public WebElement loginButton;
	
	@FindBy(id = "password")
	public WebElement passwordBar;
	
	@FindBy(id = "email")
	public WebElement emailBar;
	
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
