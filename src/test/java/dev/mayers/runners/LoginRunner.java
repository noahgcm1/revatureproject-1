package dev.mayers.runners;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import dev.mayers.pages.DashboardPage;
import dev.mayers.pages.LoginPage;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", glue = "com.revature.steps")
//features is the location of your feature files. glue is the location of your step implementations
public class LoginRunner {
	
	public static WebDriver driver;
	public static LoginPage login;
	public static DashboardPage dashboard;
	
	@BeforeClass
	public static void setUp() {
		File file = new File("src/test/resources/geckodriver.exe");
		System.setProperty("webdriver.firefox.driver", file.getAbsolutePath());
		
		driver = new ChromeDriver();
		login = new LoginPage(driver);
		dashboard = new DashboardPage(driver);
	}
	
	@AfterClass
	public static void tearDown() {
		driver.quit();
	}

}
