Feature: Logging into TRMS

	Background:
		Given The Guest is on the login page
	
	Scenario Outline: The Guest tries to log into an account
		When The user enters "<email>" and "<password>"
		Then The name shown should be "<name>"
	
	Examples:
		| email 							 | password | name 				   |
		|mumwage@company.org|	password	| Minnie Mumwage |
		|pervise@company.org 	 | password |Sue Pervise		   |
		|ager@company.org	     | password| Manny Ager	       |
		|cutive@company.org	     | password| Zeke Cutive			|